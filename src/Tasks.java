import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Tasks {
    public static void main(String[] args) {
        City[] cities = {new City("Москва"), new City("Санкт-Петербург"), new City("Воронеж")};

        Tasks tasks = new Tasks();
        System.out.println(tasks.task1(cities));

        float f = 41.7F;
        System.out.println(tasks.task2(f));

        System.out.println(tasks.task3(1048));
        System.out.println(tasks.task3(11));
        System.out.println(tasks.task3(5));
        System.out.println(tasks.task3(41));
        System.out.println(tasks.task3(25));

        System.out.println(tasks.task4(10));
        System.out.println(tasks.task4(11));
        System.out.println(tasks.task4(13));
        System.out.println(tasks.task4(2));
        System.out.println(tasks.task4(121));
        System.out.println(tasks.task4(97));

        System.out.println(Arrays.toString(tasks.task5(new Object[]{7, 17, 1, 9, 1, 17, 56, 56, 23}, new Object[]{56, 17, 17, 1, 23, 34, 23, 1, 8, 1})));
    }

    public String task1(City[] city) { // 3 min. City in the task wasn't proposed as String, so I decided to use it as Entity
        StringBuilder str = new StringBuilder();
        for (City c : city) {
            str.append(c.name()).append(",\s");
        }
        str.append("\b\b.");
        return str.toString();
    }

    public int task2(float f) { // 5 min
        int res = Math.round(f);
        int tempLow = res;
        int tempHigh = res;
        int i = 1;
        int j = 1;
        for (; i <= 5; i++) {
            if (tempLow % 5 == 0) {
                break;
            }
            tempLow--;
        }
        for (; j <= 5; j++) {
            if (tempHigh % 5 == 0) {
                break;
            }
            tempHigh++;
        }
        if (i > j) {
            return tempHigh;
        } else {
            return tempLow;
        }
    }

    public String task3(int value) { // 10 min
        String res = value + " компьютеров";
        if (value == 1) {
            return value + " компьютер";
        }
        int temp = value % 10;
        if (value <= 100) {
            switch (temp) {
                case 1 -> {
                    if (value == 11) {
                        return value + " компьютеров";
                    }
                    return value + " компьютер";
                }
                case 2, 3, 4 -> {
                    return value + " компьютера";
                }
                case 5, 6, 7, 8, 9, 0 -> {
                    return value + " компьютеров";
                }
            }
        } else {
            switch (temp) {
                case 1 -> {
                    if (String.valueOf(value).replaceAll("1", "").isEmpty()) {
                        return value + " компьютеров";
                    }
                    return value + " компьютер";
                }
                case 2, 3, 4 -> {
                    return value + " компьютера";
                }
                case 5, 6, 7, 8, 9, 0 -> {
                    return value + " компьютеров";
                }
            }
        }
        return res;
    }

    public boolean task4(int num) { // 2-3 min
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
    }

    public Object[] task5(Object[] ar1, Object[] ar2) { // 6 min. Used Object as type wasn't specified.
        ArrayList<Object> list1 = new ArrayList<>(Arrays.asList(ar1));
        ArrayList<Object> list2 = new ArrayList<>(Arrays.asList(ar2));
        HashMap<Object, Integer> map = new HashMap<>();
        for (Object o : list1) {
            if (map.containsKey(o)) {
                map.put(o, map.get(o) + 1);
            } else {
                map.put(o, 1);
            }
        }
        for (Object o : list2) {
            if (map.containsKey(o)) {
                map.put(o, map.get(o) + 1);
            } else {
                map.put(o, 1);
            }
        }
        list1.retainAll(list2);

        return list1.stream().filter(x -> map.get(x) >= 4).distinct().toArray();
    }
}
